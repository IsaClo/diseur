import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import {Container} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";

class ModifMotpasse extends React.Component{

    render() {
        return(
            <Container fluid="true" className="bg-dark">
                <Row style={{height:"100vh"}}>
                    <Col className="col-md-6 offset-md-3 pt-5 text-light font-weight-bold">
                        <Card border="info" className="bg-dark">
                            <Card.Header className="text-center font-weight-bold text-info"><h2>Création d'un nouveau compte</h2></Card.Header>
                            <Card.Body>
                                <Form>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label className="text-warning">Confirmez votre Email</Form.Label>
                                        <Form.Control type="email" placeholder="Votre email" />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label className="text-warning">Nouveau mot de passe</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label className="text-warning">Confirmez mot de passe</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>


                                    <br/>


                                    <Card.Footer>
                                        <Form.Group controlId="formBasicCheckbox" className="text-center">
                                            <Button variant="danger" type="submit">
                                                Modifier le mot de passe
                                            </Button>
                                        </Form.Group>
                                    </Card.Footer>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default ModifMotpasse;