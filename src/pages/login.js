import React from "react";
import ModifMotpasse from "./modifProfil";
import Register from "./register";
import Container from "react-bootstrap/Container";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";

class Login extends React.Component{



    render() {
        return(
                <Container fluid="true">
                    <Row className="test">
                        <Col className="bg-dark">
                        </Col>
                        <Col className="droite">
                            <Row className="center">
                                <form>
                                    <InputGroup className="mb-3 inp">
                                        <FormControl/>
                                    </InputGroup>
                                    <InputGroup className="mb-3 inp">
                                        <FormControl/>
                                    </InputGroup>
                                    <Row className="link">
                                        <Col className="size">
                                            <Link to="/login/register">Register</Link>
                                        </Col>
                                        <Col  className="size">
                                            <Link to="/login/modifMot">Mot passe oublié</Link>
                                        </Col>
                                    </Row>
                                    <Button className="btn" variant="outline-success">Login</Button>

                                </form>
                            </Row>
                        </Col>
                    </Row>
                </Container>




        );
    }
}

export default Login;