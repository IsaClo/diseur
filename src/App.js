import React from 'react';
import Login from "./pages/login";
import Accueil from "./pages/accueil";
import Profil from "./pages/profil";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Register from "./pages/register";
import ModifMotpasse from "./pages/recupMotpasse";


export default  function connexion() {
  return (
      <Router>
        <div>
          <Switch>
            <Route  path="/login/register">
              <Register/>
            </Route>
            <Route  path="/login/modifMot">
              <ModifMotpasse/>
            </Route>
            <Route path="/accueil">
              <Accueil/>
            </Route>
            <Route path="/profil">
              <Profil/>
            </Route>
            <Route  path="/">
              <Login/>
            </Route>
          </Switch>
        </div>
      </Router>
  );
}


